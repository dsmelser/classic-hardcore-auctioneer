# Overview

This directory can be used to quickly check out the needed modules and libraries for Auctioneer, Gatherer and the other Norganna's AddOns.

# Usage

Check out this repository:

```
git clone https://gitlab.com/norganna-wow/all.git
#or
git clone git@gitlab.com:norganna-wow/all.git
```

Then pull down all the submodules:

```
git submodule update --init --recursive --remote
```

Each submodules will be updated to the head of their "master" branches and put into their correctly named folders which can then be moved (or linked) 
into the `World of Warcraft/Interface/AddOns` folder.

You can also use this same command to update the entire project so that each submodule in auctioneer, gatherer, and libs points to its most recent 
master branch on the remote origin, which you might do to capture the most recent updates committed by teammates.  You should commit or stash 
any WIP changes you are working on first to avoid errors which will block the update from happening.

For reference, the above command is functionally equivalent to doing the following (checkout the defined or master branch in each submodule):
```
git submodule foreach -q --recursive 'git checkout $(git config -f $toplevel/.gitmodules submodule.$name.branch || echo master)'
```

PLEASE NOTE: Updating in this way will put any updated projects into "Detached Head" status.  This is by design, given the typical way submodules 
are used in git.  This has the potential (if you don't notice and correct it) to cause problems when you later modify or restore a stashed change, 
then try to commit, as you will not be on any branch.  Make sure to checkout a new or existing branch first before performing additional work or committing further.

Alternately, you can use one of the following commands to instead update each submodule and stay on your current branch, by allowing git to merge or rebase
the master branch updates into your current branch:
```
git submodule update --init --recursive --remote --merge
#or
git submodule update --init --recursive --remote --rebase
```

### Fetching partially

If you only wanted to fetch a subset of the modules (instead of all), you can specify the things you want direcly after the command like the following (which in this example case would only download gatherer and all the libs):

```
git submodule update --init --remote gatherer/gatherer libs
```
